package main;

public class QuickSort {

    public void quickSort(int[] values) {
        if (values == null || values.length == 0) {
            return;
        }
        sort(values, 0, values.length - 1);
    }

    private void sort(int arr[], int start, int end) {
        int pivot = partition(arr, start, end);
        if (start < pivot - 1) {
            sort(arr, start, pivot - 1);
        }
        if (pivot < end) {
            sort(arr, pivot, end);
        }
    }

    private int partition(int arr[], int start, int end) {
        int i = start, j = end;
        int tmp;
        int pivot = arr[(start + end) / 2];

        while (i <= j) {

            while (arr[i] < pivot) {
                i++;
            }

            while (arr[j] > pivot) {
                j--;
            }

            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }
        ;
        return i;
    }
}

package main;

import java.util.Random;

public class Main {

    private static int N = 23;
    private static int[] arr;
    private static Random random;

    public static void main(String[] args) {

        createArr();
        QuickSort sorter = new QuickSort();
        sorter.quickSort(arr);
        System.err.println("");

        for (int i = 0; i < arr.length; i++) {
            System.err.print(arr[i] + "   ");
        }
    }

    private static void createArr() {
        arr = new int[N];
        random = new Random();
        for (int i = 0; i < N; i++) {
            arr[i] = random.nextInt(1000);
            System.err.print(arr[i] + "   ");
        }
    }
}

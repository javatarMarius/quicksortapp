package main;

import org.junit.Test;

public class QuickSortTest {

    @Test
    public void testNull() {
        QuickSort sorter = new QuickSort();
        sorter.quickSort(null);
    }

    @Test
    public void testEmptyArr() {
        QuickSort sorter = new QuickSort();
        sorter.quickSort(new int[0]);
    }

    @Test
    public void testSingleElement() {
        int[] arr = {21};
        QuickSort sorter = new QuickSort();
        sorter.quickSort(arr);
    }

    @Test
    public void testIdenticalNumbers() {
        int[] arr = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
        QuickSort sorter = new QuickSort();
        sorter.quickSort(arr);
    }

    @Test
    public void testSortedArray() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        QuickSort sorter = new QuickSort();
        sorter.quickSort(arr);
    }

    @Test
    public void testNegativeElements() {
        int[] arr = {-100, -50, -35, -7, -10, -1, -95, -21, -2, -17, -8};
        QuickSort sorter = new QuickSort();
        sorter.quickSort(arr);
    }
}
